# icsclass-certificate-system

> ICS Class Certificate System
This system is intended for use for ICS Class staff and surveyors as administrators see fit.
This system was started on June 1, 2017, built on top of Node.js backend with Vue.js as front end and MongoDB for database management.


## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
