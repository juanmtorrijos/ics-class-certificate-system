import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Login/Home'
import NotFound from '@/components/globalComponents/NotFound'
import Vessels from '@/components/Vessels/Vessels'
import NewVessel from '@/components/Vessels/NewVessel'
import Certificates from '@/components/Certificates/Certificates'
import Authorizations from '@/components/Authorizations/Authorizations'
import Offices from '@/components/Offices/Offices'
import Surveyors from '@/components/Surveyors/Surveyors'
import Flags from '@/components/Flags/Flags'

import VueSVGIcon from 'vue-svgicon'

Vue.use(Router)

Vue.use(VueSVGIcon)

export default new Router({
	mode: 'history',
	routes: [
		{ path: '*', name: 'not-found', component: NotFound },
		{ path: '/', name: 'home', component: Home },
		{ 
			path: '/vessels',
			name: 'vessels',
			component: Vessels,
		},
		{
			path: '/certificates',
			name: 'certificates',
			component: Certificates
		},
		{
			path: '/authorizations',
			name: 'authorizations',
			component: Authorizations
		},
		{
			path: '/offices',
			name: 'offices',
			component: Offices
		},
		{
			path: '/surveyors',
			name: 'surveyors',
			component: Surveyors
		},
		{
			path: '/flags',
			name: 'flags',
			component: Flags
		}
	]
})
