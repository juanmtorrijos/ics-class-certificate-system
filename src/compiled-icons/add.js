var icon = require('vue-svgicon')
icon.register({
  'add': {
    width: 16,
    height: 16,
    viewBox: '0 0 51 51',
    data: '<path pid="0" d="M45 .5H0v43h5v-37h40z"/><path pid="1" d="M7 8.5v42h44v-42H7zm33 22H30v10a1 1 0 1 1-2 0v-10H18a1 1 0 1 1 0-2h10v-10a1 1 0 1 1 2 0v10h10a1 1 0 1 1 0 2z"/>'
  }
})
