var icon = require('vue-svgicon')
icon.register({
  'panama': {
    width: 16,
    height: 16,
    viewBox: '0 0 512 512',
    data: '<circle pid="0" cx="256" cy="256" r="256"/><path pid="1" d="M0 256c0 141.384 114.616 256 256 256V256H0z"/><path pid="2" d="M256 0c141.384 0 256 114.616 256 256H256V0z"/><path pid="3" d="M152.389 89.043l16.577 51.018h53.643l-43.398 31.53 16.576 51.018-43.398-31.531-43.398 31.531 16.576-51.018-43.398-31.53h53.643z"/><path pid="4" d="M359.611 289.391l16.577 51.018h53.643l-43.399 31.53 16.577 51.018-43.398-31.531-43.398 31.531 16.576-51.018-43.398-31.53h53.643z"/>'
  }
})
